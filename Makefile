# Common makefile for bootloader *and* the application.
#
# What you need to edit:
#   - the BOOT_SOURCES / APP_SOURCES, the corresponding
#     ..._HEADERS, and possibly ..._ASMSRCS file lists
#   - at your option, the BOOT_NAME / APP_NAME for custom naming,
#   - make your linker scripts match the BOOT_LDSCR / APP_LDSCR
#     variables (either change file names, or set variables
#     appropriately)
#
# You shouldn't have to touch anything else.


# bootloader setup
BOOT_NAME    := boot
BOOT_SOURCES := common.c boot.c
BOOT_HEADERS := common.h
BOOT_ASMSRCS := 
BOOT_LDSCR   := stm32h7_$(BOOT_NAME).ld

# application setup
APP_NAME     := mini
APP_SOURCES  := common.c mini.c
APP_HEADERS  := common.h
APP_ASMSRCS  :=
APP_LDSCR    := stm32h7_$(APP_NAME).ld

#
# You shouldn't have to touch anything below this point
#

# Generating useful variables
APP_OBJS     := $(APP_SOURCES:.c=_$(APP_NAME).o) $(APP_ASMSRCS:_.s=_$(APP_NAME).O)
APP_DEPS     := $(APP_SOURCES:.c=.d)
BOOT_OBJS    := $(BOOT_SOURCES:.c=_$(BOOT_NAME).o) $(BOOT_ASMSRCS:.s=_$(BOOT_NAME).O)
BOOT_DEPS    := $(BOOT_SOURCES:.c=.d)

# Toolchain setup
CC     := arm-none-eabi-gcc
CXX    := arm-none-eabi-c++
LD     := arm-none-eabi-ld

INCLUDE := -I.

## Need these when compiling using HAL / STM32-code
# INCLUDE += -DSTM32H743xx -DUSE_HAL_DRIVER -DDEBUG

# for dependency generation
CFLAGS_dep = -c -MMD -MP -MF"$(<:.c=.d)" -MT"$@" -MT"$(<:.c=.d)"

# CPU architectural stuff
CFLAGS_arch := \
	-mcpu=cortex-m7 \
	--specs=nano.specs \
	-mfpu=fpv5-d16 \
	-mfloat-abi=hard \
	-mthumb

# language-agnostic part of CFLAGS
CFLAGS_cmn = -c $(INCLUDE) $(CFLAGS_arch) $(CFLAGS_dep)

# specific flags only for c code compilations
CFLAGS_cc := \
	-O0 -W -Wall -Wextra -Wpedantic -Wno-unused-parameter \
	-fstack-usage \
	-ffunction-sections \
	-fdata-sections \
	-std=gnu11

# specific flags only for assembler compilation
CFLAGS_asm := -x assembler-with-cpp


LDFLAGS = \
	-mcpu=cortex-m7 \
	--specs=nosys.specs \
	-Wl,-Map="$(@:.elf=.map)" \
	-Wl,--gc-sections \
	-static \
	--specs=nano.specs \
	-mfpu=fpv5-d16 \
	-mfloat-abi=hard \
	-mthumb \
	-Wl,--start-group \
	-lc -lm \
	-Wl,--end-group


all: $(BOOT_NAME).bin $(APP_NAME).bin

.SUFFIXES:
.SUFFIXES: .c .cpp .s .elf

%_$(BOOT_NAME).o: %.c
	$(CC) $(CFLAGS_cc) $(CFLAGS_cmn) -o $@ $<

%_$(APP_NAME).o: %.c
	$(CC) $(CFLAGS_cc) $(CFLAGS_cmn) -o $@ $<

%_$(BOOT_NAME).O: %.s
	$(CC) $(CFLAGS_asm) $(CFLAGS_cmn) -o $@ $<

%_$(APP_NAME).O: %.s
	$(CC) $(CFLAGS_asm) $(CFLAGS_cmn) -o $@ $<

%.bin: %.elf
	arm-none-eabi-size "$<"
	arm-none-eabi-objdump -h -S "$<"  > "$(<:.elf=.list)"
	arm-none-eabi-objcopy  -O binary "$<" "$@"

$(BOOT_NAME).elf: $(BOOT_OBJS) $(BOOT_LDSCR)
	$(CC) -o $@ $(BOOT_OBJS) -T"$(BOOT_LDSCR)" $(LDFLAGS)

$(APP_NAME).elf: $(APP_OBJS) $(APP_LDSCR)
	$(CC) -o $@ $(APP_OBJS) -T"$(APP_LDSCR)" $(LDFLAGS)

clean:
	rm -rf $(BOOT_DEPS) $(BOOT_OBJS) $(BOOT_OBJS:.o=.su)
	rm -rf $(APP_DEPS)  $(APP_OBJS)  $(APP_OBJS:.o=.su)

	rm -rf $(BOOT_NAME) $(BOOT_NAME).map $(BOOT_NAME).elf
	rm -rf $(APP_NAME)  $(APP_NAME).map  $(APP_NAME).elf

	rm -rf $(BOOT_NAME).bin $(BOOT_NAME).list
	rm -rf $(APP_NAME).bin  $(APP_NAME).list

	rm -rf *~ \%*

-include $(BOOT_DEPS)
-include $(APP_DEPS)
