#include "common.h"

int main (void);
void (*entryptr_t)(void);

// minimal interrupt vector table
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
uint32_t *vector_table[] __attribute__((section(".isr_vector"))) = {
	(uint32_t*) (RAM_D1_BASE + RAM_D1_SIZE), // initial stack ptr
	(uint32_t*) main,     // entry point
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
#pragma GCC diagnostic pop

int main (void)
{
	// Initialize global/static vars
	__init_global(&_data_init, &_sdata, &_edata);
	__init_bss(&_sbss, &_ebss);

	__enable_irq(); // we possibly got here by bootloader, so IRQs are disabled
	
	// Enable bus clock for GPIO Port B (LEDs are on PB0, respectively PB14).
	*RCC_AHB4ENR = 0xffffffff & RCC_AHB4ENR_MASK_GPIOB;

	// Set mode set to "output" for pins 0 and 13.
	*GPIOB_MODER = (GPIOx_MODER_OUTPUT << GPIOx_MODER_PINSHIFT(0)) |
		       (GPIOx_MODER_OUTPUT << GPIOx_MODER_PINSHIFT(13));
	
	while (1) {
		*GPIOB_ODR = 1 << LED_GRN_SHIFT;
		delay_ms (500);
		
		*GPIOB_ODR = 0 << LED_GRN_SHIFT;
		delay_ms (500);
	}

	return 0;
}
