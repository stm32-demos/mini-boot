#ifndef MINIBOOT_COMMON_H
#define MINIBOOT_COMMON_H

/*
 * Header file to define some common routines and constants,
 * either for switching or for the hardware access.
 *
 * We access the STM32 hardware without HAL, so we'll be
 * doing some of the bit juggling manually. In a "real"
 * application, one would want to have some level of
 * abstraction for this. (Looking at the internals of the
 * STM32 HAL is quite instructive), but here it's distracting,
 * so we're keeping it as explicit as possible.
 */

/* Main type we'll be using :) */
typedef unsigned long uint32_t;

/*
 * Flashing address of the application is different from the bootloader.
 */
#define FLASH_BASE       0x08000000
#define BOOTLOADER_BASE  FLASH_BASE
#define APPLICATION_BASE FLASH_BASE + 0x00020000

/*
 * Runtime data (stack/heap) go into RAM. Where exactly, that we define
 * in the linker script. Currently, the linker script says "D1", but
 * we could use whatever we can get our hands on.
 *
 * Consult the H7 reference guide for which RAM is most useful for
 * which purpose (TL;DR: D1 should be used for applications, DTC
 * for bulk data like pixbuf etc; but there's nothing that speaks
 * against using DRC MRAM for applications in principle, besides
 * possibly performance [?]).
 */
#define RAM_D1_BASE 0x24000000 /* we have 128 KB of ram here */
#define RAM_D1_SIZE 128*1024

#define DTCMRAM_BASE 0x20000000 /* ...and another 512 KB of DRCMRAM */
#define DTCMRAM_SIZE 512*1024

/* Which RAM we're using is imporant for the layout of the ISR vector */
//#define RAM_BASE   RAM_D1_BASE
//#define RAM_SIZE   RAM_D1_SIZE
//#define RAM_END    (RAM_BASE + RAM_SIZE)

/*
 * I/O addresses, specific to the processor. These are for STM32-H743.
 */

/*
 * The Reset and Clock Contrl (RCC) block disables/enables clocking
 * for particular parts of the MCU. Peripherlas require a "bus interface
 * clock" to access their registers i.e. to operate (see e.g. the
 * STM32-H7 Reference Guide RM0433 p. 364).
 * Most of the peripherals are connected to the AHB bus matrix, in particular
 * the GPIO ports. So we need to access the corresponding clock-enable
 * register (ENR). Port B is in AHB4 (offset 0E0), bit 1 (i.e. mask 0x02).
 *
 */ 
#define RCC_BASE    0x58024400ul
#define RCC_AHB4ENR ((uint32_t*)(RCC_BASE + 0xE0))
#define RCC_AHB4ENR_MASK_GPIOB 0x02

/*
 * NIVC - Nested Interrupt Vector Controller
 *
 * This is an Arm Cortex M7 peripheral that controls the interrupts.
 * We need few registers to make sure we clear all pending/active
 * interrupts from the current application before jumping to another.
 */
#define NVIC_ICER_BASE 0xE000E200  /* Interrupt Clear-enable Register */
#define NVIC_ICPR_BASE 0xE000E280  /* Interrupt Clear-pending Register */

/*
 * SCB - System Control Block.
 *
 * An ARM Cortex M7 peripheral for various system control and information
 * tasks. From here we need the VTOR (vector table offset register) to
 * specify the location of the new "application" interrupt vector table.
 */
#define SCB_VTOR   0x3000ED08  /* Vector Table Offset Register */

/*
 * SysTick registers
 */
#define SYSTICK_CTRL  0xE000E010  /* SYST_CSR, Control/Status Register */

#define SYSTICK_CTRL_ENABLE     0x01l
#define SYSTICK_CTRL_CLKSOURCE  0x04l

/*
 * Red and green LEDs on the Nucleo-144 ZI2 board are on Port B
 * (PB14 and PB0, respectively).
 * Yellow LED is on port E, PE1.
 */

/* GPIO Port B       periph.base  + gpio-offs    + port-b offs. */
#define GPIOB_BASE  (0x40000000ul + 0x18020000ul + 0x0400ul)  // 0x58020400ul
#define GPIOE_BASE  (0x40000000ul + 0x18020000ul + 0x1000ul)  // 0x58021000ul

/* GPIO Mode register -- setting up function of GPIO pins */
#define GPIOB_MODER ((uint32_t*)(GPIOB_BASE + 0x00))

/* These values need to be written to GPIOx_MODER to prepare ports */
#define GPIOx_MODER_INPUT  0
#define GPIOx_MODER_OUTPUT 1  // <--- We need this to blink LEDs
#define GPIOx_MODER_ALTFUN 2
#define GPIOx_MODER_ANALOG 3

/* Returns the shift patern for pin X (2 bits per pin) */
#define GPIOx_MODER_PINSHIFT(x) (x<<1)

/* Output data register */
#define GPIOB_ODR   ((uint32_t*)(GPIOB_BASE + 0x14))

/* Masks for LEDs within the ODR register */
#define LED_GRN_MASK 0x00000001  /* Green/PB0 is on bit 0 */
#define LED_GRN_SHIFT 0
#define LED_RED_MASK 0x00002000  /* Red/PB14 is on bit 14 */
#define LED_RED_SHIFT 14


/*
 * Poor man's delay function -- just eat some CPU cycles.
 * Experiments show that on the Nucleo-144 board I'm using,
 * there are about 20 mio. cycles pers second, or 20.000 per
 * millisecond.
 */
void delay_loop (uint32_t cnt);
#define delay_ms(ms)				\
	do {					\
		delay_loop(ms*20000);		\
	} while(0)


/*
 * Some hardware specific functions -- these were mostly
 * "stolen" from the STM32 HAL.
 */

/*
 * Sets the Interrupt (I) bit in the Current Program Status Register
 * (CPSR). This allows interrupt servicing.
 */
__attribute__((always_inline)) static inline void __enable_irq()
{
	__asm volatile ("cpsie i":::"memory");
}

/*
 * Clears the Interrupt (I) bit in the Current Program Status Register
 * (CPSR). This stops interrupt servicing by the processor.
 */
__attribute__((always_inline)) static inline void __disable_irq()
{
	__asm volatile ("cpsid i":::"memory");
}

/*
 * Instruction Synchronization Barrier (ISB)
 * Flushes instruction pipeline so that any instruction
 * after this is fetched from cache or memory.
 */
__attribute__((always_inline)) static inline void  __ISB()
{
	__asm volatile ("isb 0xF":::"memory");
}

/*
 * Data Synchronization Barrier (DSB)
 * Returns only when any memory access that was triggered
 * before this instruction has completed.
 */
__attribute__((always_inline)) static inline void  __DSB()
{
	__asm volatile ("dsb 0xF":::"memory");
}

/*
 * Writes the given value to the Control Register
 * (not sure what that register does -- is it the System Control Register,
 * controlling sleep modes etc?)
 */
__attribute__((always_inline)) static inline void __set_CONTROL(uint32_t val)
{
	__asm volatile ("msr control, %0" : : "r" (val) : "memory");
}

/*
 * Sets Main Stack Pointer
 */
__attribute__((always_inline)) static inline void __set_MSP(uint32_t addr)
{
	__asm volatile ("MSR msp, %0" : : "r" (addr) : );
}


/*
 * Used to initialize global variables. Symbols are defined in linker script.
 */
extern uint32_t _data_init; // begin of the actual initialization data (on flash)
extern uint32_t _sdata, _edata;  // begin/end address of the in-memory .data section
extern uint32_t _sbss, _ebss; // begin/end of the .bss (uninitialized vars) section

/*
 * Copies data section (i.e. initialization values of global/static)
 * varsfrom flash into ram.
 */
void __init_global(uint32_t *src, uint32_t *dst, uint32_t *dst_end);

/*
 * Zeroes out BSS section (i.e. memory region for non-initialized variables)
 */
void __init_bss(uint32_t *dst, uint32_t *dst_end);


/* Bootloader jumping stuff */

/*
 * Initializes interrupt service routine (ISR) vectors.
 */
__attribute__((optimize("-O0")))
void jmp_init_vectors(uint32_t *dest, uint32_t *src, unsigned short size);

/*
 * Continues execution of a new application from 'addr'.
 */
__attribute__((optimize("-O0")))
void jmp_goto(uint32_t addr);

#endif // MINIBOOT_COMMON_H
