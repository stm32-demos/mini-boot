Minimal Bootloader Example
==========================

As the name suggests, this implements a minimal bootloader-application
architecture example for the STM32-H7 MCU.

Inspiration is taken mostly from 
[Devcoons](https://www.devcoons.com/howto-stm32-custom-bootloader-application/)
and [Carmine Noviello's book](https://www.carminenoviello.com/mastering-stm32/),
but the code is written from scratch and extensively commented. (There still
are some parts I don't understand, watch out for sharp edges.)

Build with `make`, flash with your favorite method.

The bootloader will start, flash the GPIO at PB0 once (associated with
red LED on a STM32-H743ZI2 prototyping board), after which it switches to
the main application.

The main application will flash the green LED indefinitely.
