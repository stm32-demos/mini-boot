#include "common.h"
#include <string.h>

/*
 * Poor man's delay function -- just eat some CPU cycles.
 * Experiments show that on the Nucleo-144 board I'm using,
 * there are about 20 mio. cycles pers second, or 20.000 per
 * millisecond.
 */
void delay_loop (uint32_t cnt)
{
	while (cnt--);
}      

/*
 * Initializes interrupt service routine (ISR) vectors.
 * (WHY?)
 */
__attribute__((optimize("-O0")))
void jmp_init_vectors(uint32_t *destVec, uint32_t *fromVec, unsigned short int size)
{
	__disable_irq();
	//memmove(destVec, fromVec, size);
	
	// set Vector Table Offset (VTOR) in the System Control Block (SCB)
	// scb->vtor = destVec (?)
	
	__DSB();
	__ISB();
}

/*
 * Continues execution of a new application from 'addr'
 *
 * To do this, essentially we have to:
 *
 *   - unmask (mask ok) all interrupts that may have been
 *     disabled by the bootloader
 *
 *   - tell the MCU to the new stack pointer address
 *
 *   - start code execution from new address
 */
__attribute__((optimize("-O0")))
void jmp_goto(uint32_t addr)
{
	__disable_irq();

	// disable interrupts / remove pending interrupts
	for (int i = 0; i < 8; i++) {
		((uint32_t*)NVIC_ICER_BASE)[i] = 0xffffffff;	
		((uint32_t*)NVIC_ICPR_BASE)[i] = 0xffffffff;
	}

	__set_CONTROL(0);   	        // why this? (no low-power mode?)
	__set_MSP(*(uint32_t*)addr);  	// set new stack pointer (at 0 in vector table)
	*((uint32_t*)SCB_VTOR) = addr; 	// set the location of the new vector table

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
	// entry function pointer is at index 1 in ISR
	uint32_t jmpAddr = *((volatile uint32_t*)(addr+4));
	void (*application)(void) = (void*)jmpAddr;
#pragma GCC diagnostic pop

	__ISB();
	__DSB();

	// Erasing some SysTick CTRL bits:
	//
	//  - CLKSOURCE: 0 sets external reference clock
	//  - ENABLE:    0 disables the counting of ticks
	//
	// The SysTick can be enabled to create periodic interrupts, e.g.
	// in real-time OSes. We disable it because that's something that
	// the "application" should set up, if it needs one.
	//
	// Apparently it's also used by the STM32 HAL internally, which expects
	// it to generate an interrupt every 1 ms.
	//
	// Bits:         CLKSOURCE | ENABLE
	// systick->ctrl &= ~(0x04 | 0x01);
	
	//*((uint32_t*)SYSTICK_CTRL) &= ~(SYSTICK_CTRL_CLKSOURCE | SYSTICK_CTRL_ENABLE);

	while(1) {
		application();
	}
}

void __init_global(uint32_t *src, uint32_t *dst, uint32_t *dst_end)
{
	while (dst < dst_end)
		*dst++ = *src++;
}

void __init_bss(uint32_t *dst, uint32_t *dst_end)
{
	while (dst < dst_end)
		*dst++ = 0;
}
